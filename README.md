# Open ports:

1. My docker registry is running in `5000`

2. My flaskapp is running on port `4000`

# My instance details:

* Public IP address: `13.127.55.31`

* Public DNS: `registry.manikandanravi.ml:5000`

## Steps to be followed to pull images from this repositry are:

* Login credentials are Username: `manikandan` , password: `manikandan`

* You can pull image from registry: 
```
 docker pull registry.manikandanravi.ml:5000/flaskapp/flask-app #for latest image
```